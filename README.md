# Roberte

Roberte est une stroke fonte sans courbes dessinée librement à partir des italiques de Robert Granjon. Roberte est une italique sans romain car Roberte n’en a pas besoin. Roberte est libre, Roberte est indépendante. Roberte est open source. Roberte a des caractères post-binaires. Roberte a un tracé rapide parce que Roberte n’a pas le temps de niaiser.
